import { createApp, h } from "vue";
import App from "@/App.vue";
import router from "@/router";

import createBootstrap from "@/plugins/bootstrap-vue-next";
import pinia from "@/plugins/pinia";
import "@/plugins/stream-poly";
import { i18n } from "@/plugins/vue-i18n";
import VueSelect from "@/plugins/vue-select";

import "@/assets/scss/custom.scss";

const app = createApp({
  render: () => h(App),
});

app.component("VSelect", VueSelect);

app.use(router);
app.use(createBootstrap());
app.use(pinia);
app.use(i18n);

app.mount("#app");
