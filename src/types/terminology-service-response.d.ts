export interface TerminologyServiceResponse {
  responseHeader: ResponseHeader;
  response: Response;
  facet_counts: FacetCounts;
  highlighting: { [key: string]: Highlighting };
  expanded: { [key: string]: Response };
}

export interface Response {
  numFound: number;
  start: number;
  docs: Document_[];
}

export interface Document_ {
  id: string;
  iri: string;
  short_form: string;
  label: string;
  ontology_name: string;
  ontology_prefix: string;
  type: Type;
  is_defining_ontology: boolean;
  description?: string[];
  obo_id?: string;
}

export enum Type {
  Class = "class",
  Property = "property",
}

export interface FacetCounts {
  facet_queries: unknown;
  facet_fields: FacetFields;
  facet_dates: unknown;
  facet_ranges: unknown;
  facet_intervals: unknown;
  facet_heatmaps: unknown;
}

export interface FacetFields {
  ontology_name: Array<number | string>;
  ontology_prefix: Array<number | string>;
  type: Array<number | string>;
  subset: Array<number | string>;
  is_defining_ontology: Array<number | string>;
  is_obsolete: Array<number | string>;
}

export interface Highlighting {
  label: string[];
}

export interface ResponseHeader {
  status: number;
  QTime: number;
  params: Parameters_;
}

export interface Parameters_ {
  "facet.field": string[];
  hl: string;
  fl: string;
  start: string;
  fq: string[];
  rows: string;
  "hl.simple.pre": string;
  bq: string;
  q: string;
  defType: string;
  expand: string;
  "expand.rows": string;
  "hl.simple.post": string;
  qf: string;
  "hl.fl": string[];
  facet: string;
  wt: string;
}
