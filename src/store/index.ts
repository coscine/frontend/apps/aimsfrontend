import { useLocalStorage } from "@vueuse/core";
import { defineStore } from "pinia";

import type { ApplicationProfile } from "@/types/application-profile";
import type { Dataset } from "@rdfjs/types";

import type { MainState, Theme } from "../types/types";
import ApplicationProfileImpl from "@/classes/ApplicationProfileImpl";
import State from "@/types/state";

/*  
  Store variable name is "this.<id>Store"
    id: "main" --> this.mainStore
  Important! The id must be unique for every store.
*/
export const useMainStore = defineStore({
  id: "main",
  /*  
    --------------------------------------------------------------------------------------
    STATES
    --------------------------------------------------------------------------------------
  */
  state: (): MainState => ({
    aims: {
      drafts: useLocalStorage<Record<string, ApplicationProfile>>(
        "aims.drafts",
        {},
      ),
    },
    coscine: {
      theme: useLocalStorage<Theme>("coscine.theme", "light"),
    },
  }),

  /*  
    --------------------------------------------------------------------------------------
    GETTERS
    --------------------------------------------------------------------------------------
    Synchronous code only.
    
    In a component use as e.g.:
      :label = "this.mainStore.<getter_name>;
  */
  getters: {
    currentTheme(): Theme {
      return this.coscine.theme;
    },
    draftKeys(): string[] {
      return Object.keys(this.aims.drafts);
    },
    draftValues(): ApplicationProfile[] {
      return Object.values(this.aims.drafts);
    },
  },
  /*  
    --------------------------------------------------------------------------------------
    ACTIONS
    --------------------------------------------------------------------------------------
    Asynchronous & Synchronous code comes here (e.g. API calls and VueX mutations).
    To change a state use an action.

    In a component use as e.g.:
      @click = "this.mainStore.<action_name>();
  */
  actions: {
    getAccessToken(baseUrl: string): string | undefined {
      return this.getApplicationProfile(baseUrl).accessToken;
    },
    getApplicationProfile(baseUrl: string): ApplicationProfile {
      if (this.aims.drafts[baseUrl]) {
        return this.aims.drafts[baseUrl];
      }
      return new ApplicationProfileImpl("", "", {
        definition: "[]",
        mimeType: "application/ld+json",
      });
    },
    removeApplicationProfile(applicationProfile: ApplicationProfile) {
      const drafts = { ...this.aims.drafts };
      delete drafts[applicationProfile.base_url];
      this.aims.drafts = drafts;
    },
    searchApplicationProfiles(query: string): ApplicationProfile[] {
      const queryFilter = query.toLowerCase().trim();
      return Object.values(this.aims.drafts).filter(
        (ap) =>
          ap.name.toLowerCase().trim().includes(queryFilter) ||
          ap.base_url.toLowerCase().trim().includes(queryFilter),
      );
    },
    setTheme(theme: Theme) {
      this.coscine.theme = theme;
    },
    storeApplicationProfile(
      applicationProfile: ApplicationProfile,
      dataset: Dataset,
    ) {
      const drafts = { ...this.aims.drafts };
      applicationProfile.definition = dataset.toCanonical();
      applicationProfile.mimeType = "application/n-quads";
      applicationProfile.state = State.DRAFT;
      applicationProfile.accessToken = applicationProfile.accessToken ?? "";
      drafts[applicationProfile.base_url] = applicationProfile;
      this.aims.drafts = drafts;
    },
  },
});

export default useMainStore;
