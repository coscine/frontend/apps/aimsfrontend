import {
  createRouter,
  createWebHashHistory,
  type RouteRecordRaw,
} from "vue-router";
import Home from "../views/Home.vue";
import Search from "../views/Search.vue";
import Graph from "../views/Graph.vue";
import MetadataEditor from "../views/MetadataEditor.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    redirect: "/editor",
  },
  {
    path: "/editor",
    name: "Editor",
    component: Home,
  },
  {
    path: "/search",
    name: "Search",
    component: Search,
  },
  {
    path: "/graph",
    name: "Graph",
    component: Graph,
  },
  {
    path: "/metadataEditor",
    name: "MetadataEditor",
    component: MetadataEditor,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
