/* Testing imports */
import { mount } from "@vue/test-utils";
import pinia from "@/plugins/pinia";

/* Vue i18n */
import { i18n } from "@/plugins/vue-i18n";

/* Vue Router */
import router from "@/router";

/* Vue Select */
import VueSelect from "@/plugins/vue-select";

/* Bootstrap Vue Next */
import createBootstrap from "@/plugins/bootstrap-vue-next";

/* Tested Component */
import App from "./App.vue";

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

describe("App.vue", () => {
  /* Checks for correct button validation for external users */
  test("regular", async () => {
    const wrapper = mount(App, {
      global: {
        components: {
          "v-select": VueSelect,
        },
        plugins: [pinia, i18n, router, createBootstrap()],
      },
    });

    await wrapper.vm.$nextTick();

    // Wait for 1 second until everything is set up
    await sleep(1000);

    const button = wrapper.get(`button[name="complex"]`);

    expect(button.isVisible()).toBeTruthy();

    await button.trigger("click");

    await wrapper.vm.$nextTick();

    // Wait for 1 second until everything is set up
    await sleep(1000);

    expect(wrapper.get(`#collapse-inheritance`).isVisible()).toBeTruthy();
  });
});
