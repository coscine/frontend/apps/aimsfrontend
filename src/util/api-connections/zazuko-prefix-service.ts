import type { ZazukoPrefixResponse } from "@/util/zazukoResponse";
import VocabularyTerm from "@/types/vocabulary-term";

import axios from "axios";
import { provideAdditionalInformation } from "../additionalInformationProvider";
import { getPrefixes } from "../prefixes";

export async function zazukoRequest(
  query: string,
  type: "class" | "property",
): Promise<Array<VocabularyTerm>> {
  const response = await axios.get(
    `https://prefix.zazuko.com/api/v1/search?q=${encodeURIComponent(query)}`,
  );
  let zazukoResponse: ZazukoPrefixResponse[] = response.data;
  const rdfType = "rdf:" + type.charAt(0).toUpperCase() + type.slice(1);
  const objectProperty = "owl:ObjectProperty";
  zazukoResponse = zazukoResponse.filter((vocab) =>
    vocab.parts.some(
      (part) => part.object === rdfType || part.object === objectProperty,
    ),
  );

  const prefixes = getPrefixes();
  const usedPrefixes = zazukoResponse.map((vocab) => vocab.prefixedSplitA);

  return await Promise.all(
    zazukoResponse.map(async (vocab) => {
      const additionalInformation = await provideAdditionalInformation(
        prefixes,
        vocab.iri.value,
        vocab.prefixedSplitA,
        usedPrefixes,
      );
      return {
        label: vocab.label,
        short_form: vocab.iriSplitB,
        uri: vocab.iri.value,
        prefix: vocab.prefixedSplitA,
        vocabulary: vocab.graph.value,
        description: additionalInformation.description,
        ranges: additionalInformation.ranges,
      } as VocabularyTerm;
    }),
  );
}
