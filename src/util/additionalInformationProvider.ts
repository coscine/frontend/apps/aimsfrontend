import factory from "rdf-ext";
import { retrieveVocabulary } from "./retrieveVocabulary";

import type { Prefixes } from "@zazuko/vocabularies/prefixes";
import type { Datasets } from "@zazuko/vocabularies/vocabularies";
import type { Quad } from "@rdfjs/types";

export async function provideAdditionalInformation(
  prefixes: Prefixes & { [key: string]: string },
  iriString: string,
  prefix?: string,
  usedPrefixes?: string[],
) {
  const additionalInformation = {
    description: undefined as
      | undefined
      | { en: string | undefined; de: string | undefined },
    ranges: [] as string[],
  };
  let vocabularyDatasets: Datasets = {};
  if (prefix && usedPrefixes) {
    try {
      vocabularyDatasets = await retrieveVocabulary(usedPrefixes);
    } catch (error) {
      console.error(error);
      // Vocabs cannot be loaded
    }

    if (!vocabularyDatasets[prefix]) {
      // Prefix doesn't exist
      return additionalInformation;
    }

    const vocabularyDataset = vocabularyDatasets[prefix];
    const iri = factory.namedNode(iriString);

    const rdfsComment = factory.namedNode(prefixes.rdfs + "comment");
    const dctermsDescription = factory.namedNode(
      prefixes.dcterms + "description",
    );

    additionalInformation.description = { en: undefined, de: undefined };

    const quadList: Quad[] = [];
    quadList.push(
      ...([...vocabularyDataset.match(iri, rdfsComment)] as Quad[]),
      ...([...vocabularyDataset.match(iri, dctermsDescription)] as Quad[]),
    );

    for (const quad of quadList) {
      if (quad.object.termType === "Literal") {
        let lang: "de" | "en" = "en";
        if (quad.object.language === "en" || quad.object.language === "de") {
          lang = quad.object.language;
        }
        if (additionalInformation.description[lang]) {
          additionalInformation.description[lang] += " " + quad.object.value;
        } else {
          additionalInformation.description[lang] = quad.object.value;
        }
      }
    }

    const rdfsRange = factory.namedNode(prefixes.rdfs + "range");
    const ranges = [...vocabularyDataset.match(iri, rdfsRange)] as Quad[];
    additionalInformation.ranges = ranges.map((quad) => quad.object.value);
  }
  return additionalInformation;
}
