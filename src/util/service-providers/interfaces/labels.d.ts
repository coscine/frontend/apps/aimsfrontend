/**
 * Bilingual labels
 * @export
 * @interface BilingualLabels
 */
export interface BilingualLabels {
  enPagination?: Pagination;
  /**
   * English labels
   * @type {Array<Label>}
   * @memberof BilingualLabels
   */
  en?: Array<Label> | null;
  dePagination?: Pagination;
  /**
   * German labels
   * @type {Array<Label>}
   * @memberof BilingualLabels
   */
  de?: Array<Label> | null;
}

/**
 * Bilingual labels
 * @export
 * @interface BilingualLabel
 */
export interface BilingualLabel {
  /**
   * English labels
   * @type {Label}
   * @memberof BilingualLabel
   */
  en?: Label | null;
  /**
   * German labels
   * @type {Label}
   * @memberof BilingualLabel
   */
  de?: Label | null;
}

/**
 * Label of a vocabulary entry
 * @export
 * @interface Label
 */
export interface Label {
  name?: string | null;
  value?: string | null;
}

export interface Community {
  url: string;
  name: string;
}
