import ServiceProvider from "@/util/service-providers/interfaces/service-provider";
import MockupApiConnection from "@/util/service-providers/mockup/mockup-api-connection";
import aimsLogo from "@/assets/AIMS-Logo-2-Transparent.png";

import MockupHeaderBanner from "@/components/ServiceProviders/Mockup/MockupHeaderBanner.vue";

const base_url = "https://w3id.org/nfdi4ing/";

class MockupServiceProvider implements ServiceProvider {
  apiConnection = new MockupApiConnection(base_url);
  base_url = base_url;
  base_path = "profiles/";
  can_store_metadata = true;
  filtered_search = true;
  force_base_url = false;
  logo = {
    class: "aimsLogo",
    location: aimsLogo,
  };
  name = "Mockup";
  uiElements = {
    CustomHeaderBanner: MockupHeaderBanner,
  };

  applicable(): boolean {
    return window.location.hostname.includes("localhost");
  }
  authorized(): boolean {
    return true;
  }
  getToken(): string {
    return "";
  }
}

export default MockupServiceProvider;
