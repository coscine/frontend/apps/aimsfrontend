export function isURI(uri: string) {
  // Regular expression for a simple URI
  const uriPattern =
    /^(http|https|ftp|ssh):\/\/([\d.a-z-]+\.[a-z]{2,4}|\d+\.\d+\.\d+\.\d+)(:\d+)?\/?/i;

  // Test the string against the regular expression
  return uriPattern.test(uri);
}
