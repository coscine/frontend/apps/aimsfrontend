# ActivityActions

## Props

| Prop name          | Description | Type               | Values | Default |
| ------------------ | ----------- | ------------------ | ------ | ------- |
| applicationProfile |             | ApplicationProfile | -      |         |
| definition         |             | Dataset            | -      |         |

## Events

| Event name               | Properties                                     | Description |
| ------------------------ | ---------------------------------------------- | ----------- |
| updateApplicationProfile | **&lt;anonymous1&gt;** `undefined` - undefined |
| makeEditable             |                                                |
| openEditModal            |                                                |
| textDefinitionUpdate     | **&lt;anonymous1&gt;** `undefined` - undefined |
| updateToken              |                                                |

---

```vue live
<ActivityActions
  :applicationProfile="Default Example Usage"
  :definition="Default Example Usage"
/>
```
