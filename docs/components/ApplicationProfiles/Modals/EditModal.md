# EditAP

## Props

| Prop name          | Description | Type               | Values | Default        |
| ------------------ | ----------- | ------------------ | ------ | -------------- |
| applicationProfile |             | ApplicationProfile | -      |                |
| definition         |             | Dataset            | -      |                |
| labelPrefix        |             | string             | -      | "edit"         |
| okLabelKey         |             | string             | -      | "Continue"     |
| open               |             | boolean            | -      | () =&gt; false |
| readonly           |             | boolean            | -      | false          |

## Events

| Event name  | Properties | Description |
| ----------- | ---------- | ----------- |
| close       |            |
| save        |            |
| stateChange |            |

---

```vue live
<EditAP
  :applicationProfile="Default Example Usage"
  :definition="Default Example Usage"
  :open="true"
/>
```
