# APSelection

## Props

| Prop name                        | Description | Type                            | Values | Default   |
| -------------------------------- | ----------- | ------------------------------- | ------ | --------- |
| selectedApplicationProfile       |             | ApplicationProfile \| undefined | -      | undefined |
| sessionStoredApplicationProfiles |             | number                          | -      | 0         |
| token                            |             | string                          | -      | ""        |

## Events

| Event name               | Properties | Description |
| ------------------------ | ---------- | ----------- |
| emptyApplicationProfile  |            |
| selectApplicationProfile |            |

---

```vue live
<APSelection />
```
